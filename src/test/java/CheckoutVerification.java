import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class CheckoutVerification {

    @Test
    public void checkoutVerification() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
        driver.manage().window().maximize();
        driver.get("http://autodemo.testoneo.com/en/5-women");

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));

        WebElement sizeM = driver.findElement(By.xpath("//div[@id='search_filters']//a[substring(@href, string-length(@href) - string-length('Size-M') +1) = 'Size-M']"));
        sizeM.click();

        List<WebElement> activeFilters = driver.findElements(By.xpath("//*[@class ='active_filters']//li"));
        Assert.assertEquals(activeFilters.size(), 1);
        Assert.assertTrue(activeFilters.get(0).getText().contains(": M"));

        WebElement firstArticle = driver.findElement(By.cssSelector("#js-product-list>div[class='products row']>article"));

        //Pobranie ceny artykułu
        WebElement firstArticlePrice = driver.findElement(By.cssSelector("#js-product-list>div[class='products row']>article span.price"));
        String price = firstArticlePrice.getText();
        BigDecimal unitPrice = extractAmountFromPrice(price);

        Actions action = new Actions(driver);
        action.moveToElement(firstArticle).perform();

        WebElement quickView = driver.findElement(By.cssSelector("#js-product-list>div[class='products row']>article a[class='quick-view']"));
        wait.until(elementToBeClickable(driver.findElement(By.cssSelector(".quick-view"))));
        quickView.click();

        wait.until(elementToBeClickable(driver.findElement(By.cssSelector(".product-variants"))));

        WebElement sizeSelected = driver.findElement(By.cssSelector(".product-variants option[selected='selected']"));
        //wykrycie bugu
        Assert.assertEquals(sizeSelected.getText(), "M");

        WebElement quantityWanted = driver.findElement(By.cssSelector("#quantity_wanted"));
        Assert.assertEquals(quantityWanted.getAttribute("value"), "1");
        WebElement addQuantity = driver.findElement(By.cssSelector("button.btn.btn-touchspin.js-touchspin.bootstrap-touchspin-up"));
        addQuantity.click();
        Assert.assertEquals(quantityWanted.getAttribute("value"), "2");

        WebElement addToCardButton = driver.findElement(By.cssSelector("button[data-button-action='add-to-cart']"));
        addToCardButton.click();

        wait.until(visibilityOfElementLocated(By.cssSelector(".cart-content>p:nth-child(2)")));

        WebElement totalPriceElement = driver.findElement(By.cssSelector(".cart-content>p:nth-child(2)"));
        BigDecimal totalPrice = extractAmountFromPrice(totalPriceElement.getText());

        //wykrycie bugu
        Assert.assertEquals(totalPrice, unitPrice.multiply(BigDecimal.valueOf(2)));
        WebElement quantity = driver.findElement(By.xpath("//*[@id='blockcart-modal']//p[strong='Quantity:']"));
        BigDecimal quantityValue = extractQuantity(quantity.getText());

        Assert.assertEquals(quantityValue, new BigDecimal("2"));

        WebElement proceedToCheckout = driver.findElement(By.cssSelector("[class='btn btn-primary']"));
        proceedToCheckout.click();

        wait.until(elementToBeClickable(driver.findElement(By.cssSelector(".card-block>h1"))));
        WebElement shoppingCartHeader = driver.findElement(By.cssSelector(".card-block>h1"));
        Assert.assertEquals(shoppingCartHeader.getText().toLowerCase(Locale.ROOT), "shopping cart");
    }

    private BigDecimal extractAmountFromPrice(String price) {
        Matcher matcher = Pattern.compile("([a-zA-Z\\t]+)([0-9]+(\\.[0-9]+)?)").matcher(price);
        Assert.assertTrue(matcher.find());
        String amountAsText = matcher.group(2);
        return new BigDecimal(amountAsText);
    }

    private BigDecimal extractQuantity(String quantity) {
        Matcher matcher = Pattern.compile("([0-9]+)").matcher(quantity);
        Assert.assertTrue(matcher.find());
        String quantityAsText = matcher.group(1);
        return new BigDecimal(quantityAsText);
    }
}
