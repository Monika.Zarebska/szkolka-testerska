import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

public class CartVerification {

    @Test

    public void cardVerification() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
        driver.manage().window().maximize();
        driver.get("https://autodemo.testoneo.com/");

        WebElement firstArticle = driver.findElement(By.cssSelector("#content > section > div > article:nth-child(1) > div > a"));
        //pobranie danych do końcowej asercji
        WebElement firstArticleName = driver.findElement(By.cssSelector("#content > section > div > article:nth-child(1) > div > div.product-description > h1 > a"));
        String firstArticleNameString = firstArticleName.getText();
        WebElement firstArticlePrice = driver.findElement(By.cssSelector("#content > section > div > article:nth-child(1) > div > div.product-description > div > span.price"));
        BigDecimal firstArticlePriceAmount = extractAmountFromPrice(firstArticlePrice.getText());

        Actions action = new Actions(driver);
        action.moveToElement(firstArticle).perform();

        WebElement quickView = driver.findElement(By.cssSelector("#content > section > div > article:nth-child(1) a[class='quick-view']"));
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(elementToBeClickable(driver.findElement(By.cssSelector(".quick-view"))));
        quickView.click();

        WebElement addQuantity = driver.findElement(By.cssSelector("i[class='material-icons touchspin-up']"));
        wait.until(elementToBeClickable(addQuantity));
        addQuantity.click();
        addQuantity.click();
        //pobranie danych do assercji
        WebElement sizeSelected = driver.findElement(By.cssSelector(".product-variants option[selected='selected']"));
        WebElement colorSelected = driver.findElement(By.xpath("//input[@class='input-color' and @checked='checked']/..//span[@class= 'sr-only']"));
        String colorSelectedString = colorSelected.getText();
        String sizeSelectedString = sizeSelected.getText();

        WebElement addToCart = driver.findElement(By.cssSelector("button i[class='material-icons shopping-cart']"));
        wait.until(elementToBeClickable(addToCart));
        addToCart.click();

        wait.until(visibilityOfElementLocated(By.cssSelector("#blockcart-modal")));

        WebElement closeButton = driver.findElement(By.cssSelector("#blockcart-modal button.close"));
        closeButton.click();

        WebElement cartButton = driver.findElement(By.cssSelector("#_desktop_cart"));
        wait.until(elementToBeClickable(cartButton));
        cartButton.click();


        //sprawdzam czy jestem w shopping card
        wait.until(elementToBeClickable(driver.findElement(By.cssSelector(".card-block>h1"))));
        WebElement shoppingCartHeader = driver.findElement(By.cssSelector(".card-block>h1"));
        Assert.assertEquals(shoppingCartHeader.getText().toLowerCase(Locale.ROOT), "shopping cart");

        WebElement cartItemList = driver.findElement(By.cssSelector(".cart-overview.js-cart > ul"));
        //sprawdzenie czy lista itemów = 1
        Assert.assertEquals(cartItemList.findElements(By.cssSelector("li")).size(), 1);

        //sprawdzenie danych w koszyku
        //sprawdzenie nazwy
        WebElement articleName = driver.findElement(By.cssSelector(".product-line-info>a.label"));
        Assert.assertEquals(articleName.getText().toLowerCase(), firstArticleNameString.toLowerCase());
        // sprawdzenie ceny jednostowej
        WebElement articlePrice = driver.findElement(By.cssSelector(".current-price>span.price"));
        BigDecimal articlePriceValue = extractAmountFromPrice(articlePrice.getText());
        Assert.assertEquals(articlePriceValue, firstArticlePriceAmount);
        //sprawdzenie rozmiaru
        WebElement productSize = driver.findElement(By.cssSelector("div.product-line-grid-body.col-md-4.col-xs-8 > div:nth-child(4) >span.value"));
        Assert.assertEquals(productSize.getText(), sizeSelectedString);
        //sprawdzenie koloru
        WebElement productColor = driver.findElement(By.cssSelector("div.product-line-grid-body.col-md-4.col-xs-8 > div:nth-child(5) >span.value"));
        Assert.assertEquals(productColor.getText().toLowerCase(), colorSelectedString.toLowerCase());

        //kasowanie itemów
        WebElement articleDeleteButton = driver.findElement(By.cssSelector("a.remove-from-cart"));
        articleDeleteButton.click();
        WebElement noItemsLabel = driver.findElement(By.cssSelector(".no-items"));
        Assert.assertEquals(noItemsLabel.getText(), "There are no more items in your cart");
        List<WebElement> cartItemList2 = driver.findElements(By.cssSelector(".cart-overview.js-cart > ul"));
        Assert.assertEquals(cartItemList2.size(), 0);

        WebElement continueShopping = driver.findElement(By.cssSelector(".cart-grid-body.col-xs-12.col-lg-8>a"));
        wait.until(elementToBeClickable(continueShopping));
        continueShopping.click();

        String mainPage = driver.getCurrentUrl();
        Assert.assertEquals(mainPage, "https://autodemo.testoneo.com/en/");


    }

    private BigDecimal extractAmountFromPrice(String price) {
        Matcher matcher = Pattern.compile("([a-zA-Z\\t]+)([0-9]+(\\.[0-9]+)?)").matcher(price);
        Assert.assertTrue(matcher.find());
        String amountAsText = matcher.group(2);
        return new BigDecimal(amountAsText);

    }
}
