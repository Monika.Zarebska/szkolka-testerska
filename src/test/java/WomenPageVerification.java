
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WomenPageVerification {

    @Test
    public void womanPageVerification() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://autodemo.testoneo.com/");

        WebElement clothes = driver.findElement(By.cssSelector("#category-3"));
        Actions action = new Actions(driver);
        action.moveToElement(clothes).perform();
        WebElement women = driver.findElement(By.cssSelector("#category-5"));
        action.moveToElement(women).click().perform();

        WebElement womenPageMarker = driver.findElement(By.xpath("//nav[@class='breadcrumb hidden-sm-down']//li[last()]//span"));

        Assert.assertTrue(womenPageMarker.getText().toLowerCase().contains("women"));

        WebElement womenPageMarker2 = driver.findElement(By.xpath("//*[@id='main']/div[1]/h1"));
        // System.out.println(womenPageMarker2.getText());
        Assert.assertTrue(womenPageMarker2.getText().toUpperCase().contains("WOMEN"));


    }
}
